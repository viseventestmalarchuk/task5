# Cobalt Vue Engine

This is new awesome high-performant engine for presentations.

## Components 

### Properties

Props object should look like this:

    props: {
      primitiveType: {
        type: String,
        default: "'Open Sans', sans-serif" // actualType == type by default
      },
      withActualType: {
        type: String,
        label: 'Text',
        actualType: PropType.TEXT,
        default: "Some Text"
      },
      complexActualType: {
        type: String,
        label: 'Audio',
        actualType: {
          type: PropType.FILE,
          subtype: FileType.AUDIO,
        },
        default: "./path/to/audio.mp3"
      },
      objectType: {
        type: Object,
        label: "Object",
        actualType:{
          type: PropType.OBJECT,
          subtype: {
            prop1: String,
            prop2: {
              type: String
              actualType: PropType.TEXT
            },
            prop3:{
              type: String,
              label: 'Audio',
              actualType: {
                type: PropType.FILE,
                subtype: FileType.AUDIO,
              },
            }
          }
        }
        default: () => {
          return {
            prop1: 'String bla',
            prop2: 'bla.mp3'
          }
        }
      },
      objectType: {
        type: Array,
        label: "Array",
        actualType:{
          type: PropType.Array,
          subtype: "String|Number"| {
            prop1: String,
            prop2: {
              type: String
              actualType: PropType.TEXT
            },
            prop3:{
              type: String,
              label: 'Audio',
              actualType: {
                type: PropType.FILE,
                subtype: FileType.AUDIO,
              },
            }
          }
        }
        default: () => {
          return [
            {
              prop1: 'String bla',
              prop2: 'bla.mp3'
            }
          ]
        }
      }
    }