import Vue from 'vue'
import vApp from './App.vue'
import * as settingsJson from './settings.json'

//Common components
import vText from './common/components/v-text/index.vue'
import vLayout from './common/components/v-layout/index.vue'
import vImage from './common/components/v-image/index.vue'
import vHeader from './common/components/v-header/index.vue'
import vButton from './common/components/v-button/index.vue'
import vPop from './common/components/v-popup/index.vue'
import vImageWithText from './common/components/v-image-with-text/index.vue'
import vCard from './common/components/v-card/index.vue'
import vImageGroup from './common/components/v-image-group/index.vue'
import vFooter from './common/components/v-footer/index.vue'
import vFragment from './common/components/v-fragment/index.vue'

import {
    SettingsPlugin,
} from 'vue-engine'
import { createApp } from './modules/init-editor';

Vue.use(SettingsPlugin, { settingsJson });

Vue.component('v-text', vText);
Vue.component('v-layout', vLayout);
Vue.component('v-image', vImage);
Vue.component('v-header', vHeader);
Vue.component('v-button', vButton);
Vue.component('v-popup', vPop);
Vue.component('v-image-with-text', vImageWithText);
Vue.component('v-card', vCard);
Vue.component('v-image-group', vImageGroup);
Vue.component('v-footer', vFooter);
Vue.component('v-fragment', vFragment);

createApp({
    el: '#app',
    components: {
        vApp
    },
    data: { },
})
