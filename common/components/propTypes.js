export default {
  FILE: 'file',
  STRING: 'string',
  TEXT: 'text',
  ARRAY: 'array',
  SELECT: 'enum',
  URL: 'url',
  COLOR: 'color',
  BOOLEAN: 'boolean',
  NUMBER: 'number'
}
