import { dispatchEvent } from 'utils';
import params from './params';
import { maybe } from 'folktale';
import isEditMode from './isEditMode';
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import * as settingsJson from '../../settings.json'
import { Settings } from 'vue-engine'

Vue.use(VueI18n);


class Navigator {
  constructor() {}

  public onReady(cb): void {
    cb()
  }

  public goTo(goToOptions) {}

  //TODO: remove duplication
  public goToNextSlide() {}

  public goToPrevSlide() {}

  public getNextSlide(): any {}

  public getPreviousSlide(): any {}

  public onenter(handler): void {
    setTimeout(() => {
      handler(this.getNavigation())
    }, 0)
  }

  public onleave(handler): void {}

  public getNavigation() {
    return 'app'
  }

  public getModuleId(navigation): string {
    return navigation
  }

  public getCurrentSlide() {}

  public lockNext() {}

  public lockPrev() {}

  public unlockNext() {}

  public unlockPrev() {}

  public lock() {}

  public unlock() {}

  public getNavigationStatus() {}

  public setNavigationStatus(status): void {}

  private invokeSlideEnter(slide: string): void {}

  private invokeSlideLeave(slide: string): void {}
}

export function createApp(vueConfig, navigator = new Navigator()) {
  const settings = new Settings(settingsJson).json;
  const i18n = new VueI18n({
    locale: settings.localization.current,
    messages: {}
  })

  return initEditor(params['editorPath'])
    .then(editor => editor.matchWith({
      Just: ({ value }) => value.extendVue(Vue, window['__components'], settings, navigator),
      Nothing: () => {
        Vue.use(isEditMode)
      }
    }))
    .then(() => {
      return new Vue({
        ...vueConfig,
        navigator,
        router: navigator.router,
        i18n
      })
    })
}

function initEditor(editorPath: string): Promise<maybe> {
  if (editorPath) {
    return createEditor(editorPath)
      .then(maybe.Just)
  }
  return Promise.resolve(maybe.Nothing())
}

function createEditor(editorPath) {
  return new Promise((resolve) => {
    const scriptElement = createScriptElement(editorPath);

    scriptElement.addEventListener('load', event => {
      resolve({
        extendVue: window.extendVue
      })
    })

    document.body.appendChild(scriptElement);
  })
}

function createScriptElement(src: string): HTMLElement {
  const scriptElement = document.createElement('script');
  scriptElement.setAttribute('src', src);

  return scriptElement;
}
