import { Observable } from 'rx'

export default {
  install(Vue) {
    Object.defineProperty(Vue.prototype, 'isEditMode$', {
      get: function get() {
        return Observable.just(false)
      }
    });
  }
}